//HA
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>

void do_more(FILE*, int);
int get_input(FILE*, int, int);
int get_total_lines(FILE*);
int search_str(FILE*, int, int);
int display(FILE*, int, int, int);
int open_vim(FILE* , int, int);


char* fileName;
int PAGELEN, LINELEN;
struct termios newT, oldT;


int main(int argc, char* argv[])
{
	struct winsize size;
	ioctl(0, TIOCGWINSZ, &size);
	PAGELEN = size.ws_row;
	LINELEN = size.ws_col;
	
	ioctl(0, TCGETS, &newT);
	oldT = newT;
	newT.c_lflag &= ~ECHO;
	newT.c_lflag &= ~ICANON;
	ioctl(0, TCSETS, &newT);

	FILE* fp;
	int fileCount = 1, totalLines;
	char buffer[LINELEN];
	
	if(argc == 1)
	{
		totalLines = get_total_lines(stdin);		 
		do_more(0, totalLines);
	}
	while(fileCount < argc)
	{
		fp = fopen(argv[fileCount], "r");
		if(fp == NULL)
		{
			perror("Can't Open File");					 
			exit(1);				 
		}
		fileName = argv[fileCount];
		totalLines = get_total_lines(fp);			 
		rewind(fp);		
		do_more(fp, totalLines);
		fclose(fp);
		fileCount++;
	}
	
	ioctl(0, TCSETS, &oldT);
	return 0;
}

int open_vim(FILE* fp, int currLines, int totalLines)
{

	if(fork() == 0)
	{	
		char* arg[] = {"vim", "morev1.c", NULL};
		execve("/usr//bin//vim", arg, NULL);
	}
	else
	{
		wait(NULL);
		fp = freopen(fileName, "r", fp);
		printf("\033[%dA\033[2K\033[m", PAGELEN);
	}
}

int search_str(FILE* fp, int currLines, int totalLines)
{
	int pos = ftell(fp);
	int lineCount = 1, input;
	char inputStr[LINELEN], buff[LINELEN], *result;
	ioctl(0, TCSETS, &oldT);
	printf("\033[2K \033[1G/");
	
	fgets(inputStr, LINELEN, stdin);
	
	inputStr[strlen(inputStr)-1] = '\0';
	
	while(fgets(buff, LINELEN, fp))
	{
		result = strstr(buff, inputStr);
		if(result)
		{
			break;
		}
	}
	
	if(result)
	{
		printf("\033[1A \033[2K \033[1G...Skipping\033[m\n");
		fputs(buff, stdout);
		while(fgets(buff, LINELEN, fp))
		{
			if(lineCount == PAGELEN - 1)
			{
				break;
			}
			fputs(buff, stdout);
			lineCount++;
		}
	}
	else
	{
		lseek(fileno(fp), pos, SEEK_SET);
		printf("\033[7mNo Results Found!\n\033[m");
		printf("Line Count: %d\nFD: %d\nPos: : %d\n", lineCount, fileno(fp), pos);
	}
	fflush(stdin);
	ioctl(0, TCSETS, &newT);
	return lineCount;
}

void do_more(FILE* fp, int totalLines)
{
	int lineCount = 0, currLines = 0;
	char ch, buffer[LINELEN];
	
	while(fgets(buffer, LINELEN, fp))
	{
		fputs(buffer, stdout);
		lineCount++;
		currLines++;
		if(lineCount >= PAGELEN)
		{
			lineCount = display(fp, lineCount, currLines, totalLines);
			if(lineCount == -1)
				break;
		}
	}
}

int display(FILE* fp, int lineCount, int currLines, int totalLines)
{
	FILE* fp_tty = fopen("/dev//tty", "r");
	int input = get_input(fp_tty, currLines, totalLines);

	if(input == 1)
	{	//pressed q
		printf("\033[2K \033[1G");
		lineCount = -1;
	}
	else if (input == 2)
	{
		printf("\033[2K \033[1G");
		lineCount = PAGELEN - lineCount;
	}
	else if(input == 3)
	{
		printf("\033[2K \033[1G");
		lineCount = lineCount - 1;
	}
	else if(input == 4)
	{
		lineCount = search_str(fp, currLines, totalLines);
	}
	else if(input == 5)
	{
		lineCount = open_vim(fp, currLines, totalLines);
	}
	else if(input == 0)
	{
		printf("\033[1A \033[2K \033[1G");
	}
	return lineCount;
}

int get_input(FILE* cmdStream,int currLines, int totalLines)
{
	fflush(cmdStream);
	printf("\033[7m--More--(%0.0lf%%)\033[m", (currLines/(double)totalLines) * 100);			 
	char ch = getc(cmdStream);	
	if(ch == 'q')
	{
		return 1;
	}
	else if(ch == ' ')
	{
		return 2;
	}
	else if(ch == '\n')
	{
		return 3;
	}
	else if(ch == '/')
	{
		return 4;
	}
	else if(ch == 'v')
	{
		return 5;
	}
	else
	{
		return 0;
	}

}

int get_total_lines(FILE* fp)
{
	char ch;
	int totalLines = 0;
	for(ch = getc(fp); ch != EOF; ch = getc(fp))
	{
		if(ch == '\n')
		{
			totalLines++;
		}
	}
	return totalLines;	
}
