#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

void doLS(char*);
extern int errno;

int main(int argc, char* argv[])
{
	if(argc == 1)
	{
		doLS(".");
	}
	else
	{
		int fileCount = 0;
		while(++fileCount < argc)
		{
			printf("Directory listing of %s:\n", argv[fileCount]);
			doLS(argv[fileCount]);
		}
	}
	return 0;
}

void doLS(char* dir)
{
	errno = 0;
	struct dirent *entry;
	char ch = 'A';
	
	DIR* dp = opendir(dir);
	if(dp == NULL)
	{
		perror("Cannot Open Directory");
		exit(1);
	}

	while(ch != 'z')
	{
		if(ch == 'Z')
		{
			ch = 'a';
		}
		while((entry = readdir(dp)) != NULL)
		{
			if(entry == NULL && errno != 0)
			{
				perror("Read Directory Failed");
				exit(errno);
			}
			else
			{
				if(entry->d_name[0] != '.' && entry->d_name[0] == ch)
					printf("%s\n",entry->d_name);
			}
		}
		rewinddir(dp);
		ch++;
	}
	closedir(dp);
}
