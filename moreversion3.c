#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

int PAGELEN, LINELEN;

void do_more(FILE*, int);
int get_input(FILE*, int, int);
int get_total_lines(FILE*);

int main(int argc, char* argv[])
{
	struct winsize size;
	ioctl(0, TIOCGWINSZ, &size);
	PAGELEN = size.ws_row;
	LINELEN = size.ws_col;
	
	struct termios newT, oldT;
	ioctl(0, TCGETS, &newT);
	oldT = newT;
	newT.c_lflag &= ~ECHO;
	newT.c_lflag &= ~ICANON;
	ioctl(0, TCSETS, &newT);
	
	FILE* fp;
	int fileCount = 1, totalLines;
	char buffer[LINELEN];
	
	if(argc == 1)
	{
		totalLines = get_total_lines(stdin);
		do_more(0, totalLines);
	}
	while(fileCount < argc)
	{
		fp = fopen(argv[fileCount], "r");
		if(fp == NULL)
		{
			perror("Can't Open File");					 
			exit(1);			
		}
		totalLines = get_total_lines(fp);			
		rewind(fp);
		do_more(fp, totalLines);
		fclose(fp);
		fileCount++;
	}
	
	ioctl(0, TCSETS, &oldT);
	return 0;
	
}


void do_more(FILE* fp, int totalLines)
{
	int input, lineCount = 0, currLines = 0;
	char ch, buffer[LINELEN];
	
	FILE* fp_tty = fopen("/dev//tty", "r");
	while(fgets(buffer, LINELEN, fp))
	{
		fputs(buffer, stdout);
		lineCount++;
		currLines++;
		if(lineCount == PAGELEN)
		{
			input = get_input(fp_tty, currLines, totalLines);
			if(input == 1)
			{	//pressed q
				printf("\033[2K \033[1G");
				break;	
			}
			else if (input == 2)
			{
				//pressed spacebar
				printf("\033[2K \033[1G");
				lineCount = PAGELEN - lineCount;
			}
			else if(input == 3)
			{
				//pressed enter
				printf("\033[2K \033[1G");
				lineCount = lineCount - 1;
			}
			else if(input == 4)
			{
				//pressed enter
				printf("\033[2K \033[1G");
				break;
			}
		}
	}
}

int get_input(FILE* cmdStream,int currLines, int totalLines)
{
	printf("\033[7m--More--(%0.0lf%%)\033[m", ((currLines/(double)totalLines) * 100));			 
	char ch = getc(cmdStream);	
	if(ch == 'q')
	{
		return 1;
	}
	else if(ch == ' ')
	{
		return 2;
	}
	else if(ch == '\n')
	{
		return 3;
	}
	else if(ch == '/')
	{
		printf("Line\nLines\n");
		return 4;
	}

}

int get_total_lines(FILE* fp)
{
	char ch;
	int totalLines = 0;
	for(ch = getc(fp); ch != EOF; ch = getc(fp))
	{
		if(ch == '\n')
		{
			totalLines++;
		}
	}
	return totalLines;	
}

