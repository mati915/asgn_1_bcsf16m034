#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>

char option[20];
int fileType;

void myFind(char*,char*);
void getFileType(char*);

int main(int argc,char* argv[])
{
	if (argc<=3)
	{
		printf("Invalid arguments\n");
		exit(0);
	}
	strcpy(option,argv[2]);
	if (strcmp(option,"-type")==0)
	{
		getFileType(argv[3]);
	}
	myFind(argv[1],argv[3]);	
	return 0;
}

void myFind(char* path,char* key)
{
	DIR *fp;
	struct dirent *dir;
	fp=opendir(path);
	if (fp)
	{
		while ((dir = readdir(fp))!=NULL)
		{
			if (strcmp(option,"-name")==0)
			{
				if (strcmp(dir->d_name,key)==0)
				{
					printf("%s/%s\n",path,dir->d_name);
					break;
				}
				else if (dir->d_type == DT_DIR)
				{
					if ((strcmp(dir->d_name,".") != 0) && (strcmp(dir->d_name,"..") != 0))
					{	
						char temp[50];
						strcpy(temp,path);
						strcat(temp,"/");
						strcat(temp,dir->d_name);
						myFind(temp,key);
					}
				}
			}
			else if (strcmp(option,"-type")==0)
			{	
				if (dir->d_type == fileType)
				{
					printf("%s/%s\n",path,dir->d_name);
				}
				else if (dir->d_type == DT_DIR)
				{
					if ((strcmp(dir->d_name,".") != 0) && (strcmp(dir->d_name,"..") != 0))
					{
						char temp[50];
						strcpy(temp,path);
						strcat(temp,"/");
						strcat(temp,dir->d_name);
						myFind(temp,key);
					}
				}
			}
		}
		closedir(fp);
	}

}
void getFileType(char* p)
{
	if (p[0]=='-')
		fileType=8;
	else if (p[0]=='d')
		fileType=4;	
	else if (p[0]=='b')
		fileType=6;
	else if (p[0]=='c')
		fileType=2;
	else if (p[0]=='p')
		fileType=1;	
	else if (p[0]=='l')
		fileType=10;
	else if (p[0]=='s')
		fileType=0;
}
