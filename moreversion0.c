//morev4
//handling io redirection
//multiple file names
#include <stdio.h>
#include <stdlib.h>
#define LINELEN 512
#define PAGELEN 20
void do_more(FILE *);
int get_input(FILE *);
int main(int argc , char *argv[]){
   if (argc == 1){
       do_more(stdin);
   }
   FILE * fp;
   int i=0;
   while(++i<argc)
{
   fp = fopen(argv[1] , "r");
   if(fp == NULL){
       perror("Can't open file");
       exit(1);
   }
   do_more(fp);
   fclose(fp);
   } 
   return 0;
}

void do_more(FILE *fp){
   char buffer[LINELEN];
   int num_of_lines=0;
   FILE* fp_tty=fopen("/dev//tty", "r");
   int rv;
   while(fgets(buffer, LINELEN, fp)){
      fputs(buffer, stdout);
      num_of_lines++;
      if(num_of_lines==PAGELEN)
      {
	      rv=get_input(fp_tty);
	      if(rv==0){
		printf("\033[1A \033[2K \033[1G");
		      break;	}
		else if(rv==1){
			num_of_lines -=PAGELEN;
			printf("\033[1A \033[2K \033[1G") ; }
		else if(rv==2)
{
			printf("\033[1A \033[2K \033[1G");
			num_of_lines -=1;
} 
	      else if(rv==3){
printf("\033[1A \033[2K \033[1G");
		      break;
}

      }
   }
}
int get_input(FILE* cmdstream)
{
	int c;
	printf("\033[7m --more--(%%) \033[m");
	c=getc(cmdstream);
	if(c=='q')
		return 0;
	if(c==' ')
		return 1;
	if(c=='\n')
		return 2;
	return 0;
 

}

